﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {

    private float HP;
    private float Defense;
    private float Attack;
    private double Money;

    public UnityEngine.UI.Text HP_Text;
    public UnityEngine.UI.Text Def_Text;
    public UnityEngine.UI.Text Att_Text;

    private int attack = 10;
    private int shield = 5;


    void GenerateActiveParameters()
    {
        HP_Text.text = HP.ToString();
        Def_Text.text = Defense.ToString();
        Att_Text.text = Attack.ToString();


    }

	// Use this for initialization
	void Start () {
        HP = 120.0f;
        Defense = 3.0f;
        Attack = 5.0f;
        Money = 0.0f;
        GenerateActiveParameters();

    }
	
	// Update is called once per frame
	void Update () {

        GenerateActiveParameters();
        
    }

}

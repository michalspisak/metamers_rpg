﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

    public GameObject InventoryCanvas;
    public GameObject TextToDesactivate;
    int vvd=0;

    // Use this for initialization
    void Start () {

        InventoryCanvas.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        Star_StopInventory();
    }




    void Star_StopInventory()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            InventoryVisible();
            vvd++;
        }

    }

    void InventoryVisible()
    {

        if(vvd % 2 == 1)
        {
            InventoryCanvas.SetActive(false);
            TextToDesactivate.SetActive(false);
        }
        if (vvd % 2 == 0)
        {
            InventoryCanvas.SetActive(true);
        }


    }

    void Awake()
    {
        
    }
}

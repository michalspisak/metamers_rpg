﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quest1Buttons : MonoBehaviour {

    public GameObject ThePlayer;
    public GameObject UIQuest;
    public GameObject ActiveQuestBox;
    public GameObject Objective01;
    public GameObject Objective02;
    public GameObject Objective03;
    public GameObject Objective04; // to decline
    public AudioSource AudioQuestAccept;
    public AudioSource AudioToDescription;
    public GameObject ObjectToDisactive1;
    public GameObject ObjectToDisactive2;
    public GameObject ObjectToDisactive3;


    public void AcceptQuest()
    {
        ThePlayer.SetActive(true);
        UIQuest.SetActive(false);
        StartCoroutine(SetQuestUI());
    }

    public void DeclineQuest()
    {
        ThePlayer.SetActive(true);
        UIQuest.SetActive(false);
        StartCoroutine(DeclineQuestUI());
    }


    IEnumerator SetQuestUI()
    {
        ObjectToDisactive1.SetActive(false);
        ObjectToDisactive2.SetActive(false);
        ObjectToDisactive3.SetActive(false);

        AudioQuestAccept.Play();
        ActiveQuestBox.GetComponent<Text>().text = "My First weapon";
        Objective01.GetComponent<Text>().text = "Find forest and check the chest <=";
        Objective02.GetComponent<Text>().text = "Give sword to the board where did you accept the quest <=";
        Objective03.GetComponent<Text>().text = "Enjoy your first WEAPON! <=";
        QuestManager.ActiveQuestNumber = 1;
        yield return new WaitForSeconds(0.5f);
        ActiveQuestBox.SetActive(true);
        yield return new WaitForSeconds(2);
        Objective01.SetActive(true);
        AudioToDescription.Play();
        yield return new WaitForSeconds(1);
        Objective02.SetActive(true);
        yield return new WaitForSeconds(1);
        Objective03.SetActive(true);
        yield return new WaitForSeconds(9);
        ActiveQuestBox.SetActive(false);
        Objective01.SetActive(false);
        Objective02.SetActive(false);
        Objective03.SetActive(false);

    }
    IEnumerator DeclineQuestUI()
    {
        Objective04.GetComponent<Text>().text = "Try later...";
        yield return new WaitForSeconds(1);
        Objective04.SetActive(true);
        yield return new WaitForSeconds(9);
        Objective04.SetActive(false);
    }
}

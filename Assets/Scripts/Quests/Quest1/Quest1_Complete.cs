﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quest1_Complete : MonoBehaviour {

    public float TheDistance;
    public GameObject ActionDisplay;
    public GameObject ActionText;
    public GameObject UIQuest;
    public GameObject ThePlayer;
    public GameObject ObjectiveToActivate;
    public GameObject CompleteTrigger;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        TheDistance = PlayerCasting.DistanceFromTarget;
	}

    void OnMouseOver()
    {
        if( TheDistance <= 3)
        {
            ActionDisplay.SetActive(true);
            ActionText.SetActive(true);
            ActionDisplay.GetComponent<Text>().text = "Complete Quest ";
        }

        if (Input.GetButtonDown("Action"))
        {
            if(TheDistance <= 3)
            {
                ObjectiveToActivate.SetActive(false);
                GlobalExp.CurrentExp += 200;
                GlobalExp.CurrentGold += 100;
                ActionText.SetActive(false);
                ActionDisplay.SetActive(false);
                CompleteTrigger.SetActive(false);
                QuestManager.ActiveQuestNumber -= 1;

            }
        }
    }

    void OnMouseExit()
    {
        ActionDisplay.SetActive(false);
        ActionText.SetActive(false);
    }
}

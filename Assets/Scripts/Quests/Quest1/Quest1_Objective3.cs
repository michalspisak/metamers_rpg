﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quest1_Objective3 : MonoBehaviour {

    public float TheDistance;
    public GameObject FakeSword;
    public GameObject ActionDisplay;
    public GameObject ActionText;
    public GameObject TheObjective;
    public int CloseObjective;
    public GameObject ChestBlock;

    public GameObject QuestComplete;
    public GameObject ExMark;
	
	// Update is called once per frame
	void Update () {
        TheDistance = PlayerCasting.DistanceFromTarget;

        if (CloseObjective == 3)
        {
            if (TheObjective.transform.localScale.y <= 0.0f)
            {
                CloseObjective = 0;
                TheObjective.SetActive(false);
            }
            else
            {
                TheObjective.transform.localScale -= new Vector3(0.0f, 0.0f, 0.0f);
            }

        }
	}

    void OnMouseOver()
    {
        if(TheDistance <=3)
        {
            ActionText.GetComponent<Text>().text = "Take Sword";
            TheObjective.GetComponent<Text>().text = "QUEST1 / Congratz, back to return the SWORD";
            ActionText.SetActive(true);
            ActionDisplay.SetActive(true);
        }

        if(Input.GetButtonDown("Action"))
        {
            if(TheDistance <= 3)
            {
                this.GetComponent<BoxCollider>().enabled = false;
                FakeSword.SetActive(false);
                ChestBlock.SetActive(true);
                CloseObjective = 3;
                StartCoroutine("ReturnSword");
                ActionText.SetActive(false);
                ActionDisplay.SetActive(false);
                QuestComplete.SetActive(true);
                ExMark.SetActive(true);
                }
        }
    }

    void OnMouseExit()
    {
        ActionDisplay.SetActive(false);
        ActionText.SetActive(false);
    }

    IEnumerator ReturnSword()
    {
        TheObjective.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        CloseObjective = 1;
    }
}

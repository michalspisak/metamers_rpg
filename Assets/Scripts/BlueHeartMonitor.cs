﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueHeartMonitor : MonoBehaviour {

    public static int BlueHeartValue;
    public int InternalBlueHeart;
    public GameObject BlueHeartOne;
    public GameObject BlueHeartTwo;
    public GameObject BlueHeartThree;
    public GameObject BlueHeartFour;
    public GameObject BlueHeartFive;
    public GameObject BlueHeartSix;
    public GameObject BlueHeartSeven;
    public GameObject BlueHeartEight;
    public GameObject BlueHeartNine;
    public GameObject BlueHeartTen;

    // Use this for initialization
    void Start () {

    BlueHeartValue = 0;

}
	
	// Update is called once per frame
	void Update () {

        InternalBlueHeart = BlueHeartValue;

        if(BlueHeartValue == 1)
        {
            BlueHeartOne.SetActive(true);
        }
        if (BlueHeartValue == 2)
        {
            BlueHeartTwo.SetActive(true);
        }
        if (BlueHeartValue == 3)
        {
            BlueHeartThree.SetActive(true);
        }
        if (BlueHeartValue == 4)
        {
            BlueHeartFour.SetActive(true);
        }
        if (BlueHeartValue == 5)
        {
            BlueHeartFive.SetActive(true);
        }
        if (BlueHeartValue == 6)
        {
            BlueHeartSix.SetActive(true);
        }
        if (BlueHeartValue == 7)
        {
            BlueHeartSeven.SetActive(true);
        }
        if (BlueHeartValue == 8)
        {
            BlueHeartEight.SetActive(true);
        }
        if (BlueHeartValue == 9)
        {
            BlueHeartNine.SetActive(true);
        }
        if (BlueHeartValue == 10)
        {
            BlueHeartTen.SetActive(true);
        }

    }
}

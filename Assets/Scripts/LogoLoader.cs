﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogoLoader : MonoBehaviour
{

    public void Start()
    {
        StartCoroutine(TimeFour());
    }



    IEnumerator TimeFour()
    {
        yield return new WaitForSeconds(5);
        LoadScene("MainMenu");

    }

    private void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

}

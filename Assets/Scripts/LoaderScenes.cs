﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoaderScenes : MonoBehaviour {

    public void Start()
    {
      
    }


    public void ChangeSceneToGame()
    {
         
        StartCoroutine(TimeFive());

    }

    IEnumerator TimeFive()
    {
        yield return new WaitForSeconds(1);
        LoadScene("Game");

    }

    IEnumerator TimeFour()
    {
        yield return new WaitForSeconds(5);
        LoadScene("MainMenu");

    }

    private void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

}

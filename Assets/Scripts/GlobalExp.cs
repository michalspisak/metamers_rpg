﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalExp : MonoBehaviour {

    public static int CurrentExp;
    public int InternalExp;
    public static int CurrentGold;
    public int InternalGold;
	
	// Update is called once per frame
	void Update () {

        InternalExp = CurrentExp;
        InternalGold = CurrentGold;
		
	}
}

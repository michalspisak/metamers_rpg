﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveQuest : MonoBehaviour {

    public GameObject ActiveQuests;
		
	
	
	// Update is called once per frame
	void Update () {

        ActiveQuests.GetComponent<Text>().text = "Total: " + QuestManager.ActiveQuestNumber + " Quests";

    }
}

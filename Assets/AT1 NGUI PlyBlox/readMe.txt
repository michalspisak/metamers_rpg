Thanks for your purchase,

==============================
Installation Instructions:

You must own a license of PlyGame and have it installed on your project.

You must own a license of Ngui and have it installed on your project.

Then you can install these custom Actions and start using them.

For events Listener. You can assess them in NGUI menu inside Blox (Left side of blox Editor).
For Actions you can assess in NGUI (Right Side of blox Editor)

These actions has been tested on PlyGame 2.1.5 and Ngui 3.6.2 on Unity 4.5

===============================

We have been enjoying with custom actions for PlyGame and Blox in our actually games. It just save us a ton of time in GUI design and setup NGUI.
Finally, we decided to share these PlyGame actions to community with a hope to help "Developer life easier".

These included actions in this package are very handy PlyGame Actions for NGUI and its components.
Simply, you can unlock powerful NGUI and Plygame for RPG Game features without a single line of code.

Every action has it own ToolTip to explained what it does. Please read.
You also can have a quick look in sampleScene that is already setup with these actions.

If you want to expand more features in each actions then feel free to do so. Every action script is well documented and highly optimised for Mobile, Desktop and any of Platform that is supported by PlyGame and NGUI.

Enjoy and please wait for more actions are coming.

If you need any support, suggestion or more actions please email us: 
atonevn@gmail.com
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TotalExp : MonoBehaviour {

    public GameObject totalExp;
    public GameObject totalGold;

	// Update is called once per frame
	void Update () {
        totalExp.GetComponent<Text>().text = "" + GlobalExp.CurrentExp;
        totalGold.GetComponent<Text>().text = "" + GlobalExp.CurrentGold;

    }
}
